const express=require("express");
const cors=require("cors");
const mongoose=require("mongoose");
const app=express();
const bodyParser=require("body-parser");
const postsRoutes=require("./routes/posts");
const path=require("path");

app.use(bodyParser.json());
app.use(cors());
app.use("/images",express.static(path.join("../backend/images")))
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.zsm2y.mongodb.net/test", { useNewUrlParser: true,useUnifiedTopology:true } )
.then(()=>{
    console.log("connected to DB")
})
.catch(()=>{
    console.log("connection failed")
})


app.use("/api/posts",postsRoutes)

module.exports=app;